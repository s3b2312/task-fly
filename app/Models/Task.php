<?php

namespace App\Models;

class Task extends GenericModel
{
    protected $fillable = ['title','body'];
}
